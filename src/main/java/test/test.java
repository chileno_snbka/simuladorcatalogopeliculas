package test;

import business.*;
import java.util.Scanner;

public class test {

    private static final Scanner scanner = new Scanner(System.in);
    private static int opcion = -1;
    private static final String nameFile = "C:\\Users\\agari\\Documents\\catalogoPeliculas\\peliculas.txt";
    private static final CatalogoPeliculas catalogo_peliculas = new CatalogoPeliculasImpl();

    public static void main(String[] args) {
        while (opcion != 0) {
            try {
                System.out.println("Elije opcion: \n1.- Iniciar catalogo peliculas"
                        + "\n2.- Agregar pelicula"
                        + "\n3.- Listar peliculas"
                        + "\n4.- Buscar pelicula"
                        + "\n0.- Salir");

                opcion = Integer.parseInt(scanner.nextLine());

                switch (opcion) {
                    case 1:
                        catalogo_peliculas.initFile(nameFile);
                        break;
                    case 2:
                        System.out.println("Introduzca nombre pelicula a agregar");
                        String nameMovie = scanner.nextLine();
                        catalogo_peliculas.addMovie(nameMovie, nameFile);
                        break;
                    case 3:
                        catalogo_peliculas.listMovie(nameFile);
                        break;
                    case 4:
                        System.out.println("Introduzca nombre pelicula a buscar:");
                        String searchMovie = scanner.nextLine();
                        catalogo_peliculas.searchMovie(nameFile, searchMovie);
                        break;
                    case 0:
                        System.out.println("Hasta pronto!");
                        break;
                    default:
                        System.out.println("Opcion no reconocida");
                        break;
                }

            } catch (Exception ex) {
                System.out.println("Error!");
            }
        }
    }
}
