package datos;

import domain.*;
import exception.*;
import java.util.List;

public interface AccesoDatos {
    
    Boolean file_exist(String fileName) throws AccesoDatosEx;
    
    public List<Movie> list(String fileName) throws LecturaDatosEx;
    
    void write(Movie movie, String fileName, boolean anexar) throws EscrituraDatosEx;
    
    public String search(String fileName, String search) throws LecturaDatosEx;
    
    public void create(String fileName) throws AccesoDatosEx;
    
    public void delete(String fileName) throws AccesoDatosEx;
}
