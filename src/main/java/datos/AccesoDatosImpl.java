package datos;

import domain.Movie;
import exception.AccesoDatosEx;
import exception.EscrituraDatosEx;
import exception.LecturaDatosEx;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class AccesoDatosImpl implements AccesoDatos {

    @Override
    public Boolean file_exist(String fileName) throws AccesoDatosEx {
        File file = new File(fileName);
        return file.exists();
    }

    @Override
    public List<Movie> list(String fileName) throws LecturaDatosEx {
        File file = new File(fileName);
        List<Movie> movies = new ArrayList();
        try {
            BufferedReader input = new BufferedReader(new FileReader(file));
            String line = null;
            line = input.readLine();
            while (line != null) {
                Movie movie = new Movie(line);
                movies.add(movie);
                line = input.readLine();
            }
            input.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return movies;
    }

    @Override
    public void write(Movie movie, String fileName, boolean anexar) throws EscrituraDatosEx {
        File file = new File(fileName);
        try {
            PrintWriter output = new PrintWriter(new FileWriter(file, anexar));
            output.println(movie.toString());
            output.close();
            System.out.println("Se ha escrito correctamente el archivo");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public String search(String fileName, String search) throws LecturaDatosEx {
        File file = new File(fileName);
        String result = null;

        try {
            BufferedReader input = new BufferedReader(new FileReader(file));
            String line = null;
            int i = 0;
            line = input.readLine();
            while (line != null) {
                if (search != null && search.equalsIgnoreCase(line)) {
                    result = "Pelicula " + line + " encontrada en indice " + i;
                    break;
                }
                line = input.readLine();
                i++;
            }
            input.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return result;
    }

    @Override
    public void create(String fileName) throws AccesoDatosEx {
        File file = new File(fileName);
        try {
            PrintWriter output = new PrintWriter(new FileWriter(file));
            output.close();
            System.out.println("Se ha creado el archivo correctamente");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void delete(String fileName) throws AccesoDatosEx {
        File file = new File(fileName);
        file.delete();
        System.out.println("Se ha borrado el archivo correctamente");
    }
}
