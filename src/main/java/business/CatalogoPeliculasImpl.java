package business;

import datos.*;
import domain.*;
import exception.AccesoDatosEx;
import exception.LecturaDatosEx;
import java.util.List;

public class CatalogoPeliculasImpl implements CatalogoPeliculas {

    private final AccesoDatos datos;

    public CatalogoPeliculasImpl() {
        this.datos = new AccesoDatosImpl();
    }

    @Override
    public void addMovie(String nameMovie, String nameFile) {
        Movie movie = new Movie(nameMovie);
        boolean anexar = false;
        try {
            anexar = datos.file_exist(nameFile);
            datos.write(movie, nameFile, anexar);
        } catch (AccesoDatosEx ex) {
            System.out.println("Error de acceso a datos");
            ex.printStackTrace();
        }

    }

    @Override
    public void listMovie(String fileName) {
        try {
            List<Movie> movies;
            movies = datos.list(fileName);
            for (Movie movie : movies) {
                System.out.println("Pelicula: " + movie);
            }
        } catch (AccesoDatosEx ex) {
            System.out.println("Error de acceso de datos");
            ex.printStackTrace();
        }
    }

    @Override
    public void searchMovie(String fileName, String search) {
        String result = null;
        try {
            result = datos.search(fileName, search);
        } catch (LecturaDatosEx ex) {
            System.out.println("Error al buscar pelicula");
            ex.printStackTrace();
        }

    }

    @Override
    public void initFile(String fileName) {
        try {
            if (datos.file_exist(fileName)) {
                datos.delete(fileName);
                datos.create(fileName);
            } else {
                datos.create(fileName);
            }
        } catch (AccesoDatosEx ex) {
            System.out.println("Error de acceso a datos");
            ex.printStackTrace();
        }
    }
}