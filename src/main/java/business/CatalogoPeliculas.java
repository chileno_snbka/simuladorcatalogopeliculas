package business;

public interface CatalogoPeliculas {
    
    public void addMovie(String nameMovie, String nameFile);
    
    public void listMovie(String fileName);
    
    public void searchMovie(String fileName, String search);
    
    public void initFile(String fileName);
            
}
